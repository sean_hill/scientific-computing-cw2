# Written by Sean P. Hill (sh17512@bristol.ac.uk) 2019

import numpy as np
import pylab as plt
from math import pi
from scipy.sparse import diags
from numpy.linalg import inv
from numpy.linalg import solve as linalgSolver
import sys
from pdeSolver import *

def u_I(x, L):
    # initial temperature distribution
    y = np.sin(pi*x/L) 
    return y

def u_exact(x, t, L, kappa):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y

def heat_source_test(x,t,mx):

    return 2*x*t

def test_intial_bound_conds(x,t,mxs):
    
    return [np.sin(pi*2*t)]

def test_final_bound_conds(x,t,mxs):
    
    return [np.sin(pi*2*t)]

def euc_Errors(	diff_coefficient, 
	            L,
	            T, 
	            init_temp_dist, 
	            mx,
	            mt,
	            u0,
	            ut,
	            scheme
				):


	# Working out the error using the root of the sum of the squares
	# of the distance between the respective points on the solution 
	# evaluated using the function, and the exact solution,

	error = 0
	x, u_j = PDEsolver(diff_coefficient,L,T,init_temp_dist,mx,mt,initial_boundary = 0 ,final_boundary = 0 ,boundary_conditions = "NH_Dirichlet", scheme=scheme,plot = False)
	exact_sol = u_exact(x,T,L,diff_coefficient)

	# for i in range(0, mx+1):
		
	# 	error += (u_j[i] - exact_sol[i])**2

	# euc_error = error#np.sqrt(error)

	euc_error = np.sqrt(sum((u_j-exact_sol)**2))

	return euc_error

def trap_Errors(	diff_coefficient, 
		            L,
		            T, 
		            init_temp_dist, 
		            mx,
		            mt,
		            u0,
		            ut,
		            scheme
					):
	

	# Working out the error using the difference between the areas
	# underneath the graphs using the trapezium rule.

	error = 0
	x, u_j = PDEsolver(diff_coefficient,L,T,init_temp_dist,mx,mt,initial_boundary = 0 ,final_boundary = 0 ,boundary_conditions = "NH_Dirichlet", scheme=scheme, plot = False)
	exact_sol = u_exact(x,T,L,diff_coefficient)

	for i in range(len(u_j)):
		exact_sol[i] = exact_sol[i]**2
		u_j[i] = u_j[i]**2

	exact_trap = 0.5 * (x[1]-x[0]) * (exact_sol[0] + 2*np.sum(exact_sol[1:-1]) + exact_sol[-1])
	solver_trap = 0.5 * (x[1]-x[0]) * (u_j[0] + 2*np.sum(u_j[1:-1]) + u_j[-1])

	trap_error = np.sqrt(abs(solver_trap - exact_trap))

	return trap_error

n = 3
diff_coefficient=1.0
L=1.0
T=0.5
init_temp_dist=u_I
mx=1000
#mt=10000
u0=0
ut=0
i=0

mt_array = []
FE_euc_error_array = []
BE_euc_error_array = []
CK_euc_error_array = []
FE_trap_error_array = []
BE_trap_error_array = []
CK_trap_error_array = []

for mt in np.logspace(2,3,10):

	mt = int(mt)
	mt_array.append(1/mt)

	#FE_euc_error = euc_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,forwardEuler)
	BE_euc_error = euc_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,backwardEuler)
	CK_euc_error = euc_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,crankNicholson)
	#FE_euc_error_array.append(FE_euc_error)
	BE_euc_error_array.append(BE_euc_error)
	CK_euc_error_array.append(CK_euc_error)

	# # FE_trap_error = trap_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,forwardEuler)
	# BE_trap_error = trap_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,backwardEuler)
	# CK_trap_error = trap_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,crankNicholson)
	# # FE_trap_error_array.append(FE_trap_error)
	# BE_trap_error_array.append(BE_trap_error)
	# CK_trap_error_array.append(CK_trap_error)

	"""print("\nEuclidean Distance: mt = %.0f, DeltaT = %f"%(mt, T/mt))
	print("\nForward Euler Euclidean Error:",FE_euc_error)
	print("Backward Euler Euclidean Error:",BE_euc_error)
	print("Crank Nicholson Euclidean Error:",CK_euc_error)"""

	i+=1

# for mx in np.logspace(2,3,10):

# 	mt = int(mt)
# 	mt_array.append(1/mt)

# 	#FE_euc_error = euc_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,forwardEuler)
# 	BE_euc_error = euc_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,backwardEuler)
# 	CK_euc_error = euc_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,crankNicholson)
# 	#FE_euc_error_array.append(FE_euc_error)
# 	BE_euc_error_array.append(BE_euc_error)
# 	CK_euc_error_array.append(CK_euc_error)

# 	# # FE_trap_error = trap_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,forwardEuler)
# 	# BE_trap_error = trap_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,backwardEuler)
# 	# CK_trap_error = trap_Errors(diff_coefficient,L,T,init_temp_dist,mx,mt,u0,ut,crankNicholson)
# 	# # FE_trap_error_array.append(FE_trap_error)
# 	# BE_trap_error_array.append(BE_trap_error)
# 	# CK_trap_error_array.append(CK_trap_error)

# 	"""print("\nEuclidean Distance: mt = %.0f, DeltaT = %f"%(mt, T/mt))
# 	print("\nForward Euler Euclidean Error:",FE_euc_error)
# 	print("Backward Euler Euclidean Error:",BE_euc_error)
# 	print("Crank Nicholson Euclidean Error:",CK_euc_error)"""

# 	i+=1

# fig1 = plt.figure(figsize = [12,5])
fig1 = plt.figure(figsize = [6,5])
ax1 = fig1.add_subplot(1,1,1)
# ax1.loglog(mt_array,FE_euc_error_array,label='Forward Euler')
ax1.loglog(mt_array,BE_euc_error_array,label='Backward Euler')
ax1.loglog(mt_array,CK_euc_error_array,label='Crank Nicholson')  
# ax1.set_xscale('log') 
# ax1.set_yscale('log') 
ax1.set_title("Errors of Different Numerical Schemes")
ax1.set_xlabel('Time Step')
ax1.set_ylabel('Error for Solver Against Exact Solution')
ax1.legend(loc='upper left')

# ax1 = fig1.add_subplot(1,2,2)
# # ax1.loglog(mt_array,FE_trap_error_array,label='Forward Euler')
# ax1.loglog(mt_array,BE_trap_error_array,label='Backward Euler')
# ax1.loglog(mt_array,CK_trap_error_array,label='Crank Nicholson')  
# # ax1.set_xscale('log') 
# # ax1.set_yscale('log') 
# ax1.set_title("Errors of Different Numerical Schemes")
# ax1.set_xlabel('mx')
# ax1.set_ylabel('Error Against Exact Solution Using Trapezium Rule')
# ax1.legend(loc='upper right')
plt.show()





