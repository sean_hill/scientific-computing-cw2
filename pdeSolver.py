# Written by Sean P. Hill (sh17512@bristol.ac.uk) 2019

# An implementation of the Forward Euler, Backward Euler and Crank Nicholson
# numerical schemes to solves parabolics partial differential equations (PDEs).

# Care has been taken to ensure that the code works with homogenous and non-
# homogenous dirichlet, and Neumann boundayr conditins, as well as with 
# an additive function on the right-hand side of the PDE, which could 
# represent a heat source in the heat equation. 

# Sample usage of the function

# PDEsolver(  diff_coefficient=1.0, 
#             L=1.0,
#             T=0.5, 
#             init_temp_dist=u_I,
#             mx=10,
#             mt=1000,
#             initial_boundary = 0,# 0,
#             final_boundary = 0,#0,
#             boundary_conditions = "Neumann",
#             scheme = forwardEuler,
#             heat_source = heat_source_test,
#             plot = True
#         )

import numpy as np
import pylab as plt
from math import pi
from scipy.sparse import diags
from scipy.sparse.linalg import spsolve
import sys
from numerical_schemes import *


##################################################
############### NUMERICAL SCHEMES ################
##################################################

def forwardEuler(x, t, mx, mt, lmbda, L, T, u_j, initial_boundary, final_boundary, boundary_conditions, heat_source):

    """    
    A function that applies the forward Euler numerical scheme.

    Inputs
    ----------
    x                   :   list
                            The array of x values that are used to solve the PDE, coming 
                            from the domain.

    t                   :   list
                            The array of time values that are used to solve the PDE.

    mx                  :   int
                            The number of discretisation points/grid spacings in space.

    mt                  :   int
                            The number of discretisation points/grid spacings in time.

    lmbda               :   float
                            The mesh fourier number.

    L                   :   float
                            The size of the domain.

    T                   :   float
                            The desired computational time.

    u_j                 :   numpy.array 
                            The initial range of values for the parabolic PDE.

    initial_boundary    :   int/float/Function Handle
                            The boundary condition for the initial value of the system (p).

    final_boundary      :   int/float/Function Handle
                            The boundary condition for the final value of the system (q).

    boundary_conditions :   string
                            Chooes the type of boundary conditions used in the scheme.
                            "NH_Dirichlet": Applies the non-homogenous Dirichlet boundary
                                            conditions
                            "Neumann":      Applies the Neumann boundary conditions  

    heat_source         :   Function Handle
                            Function containing the function F(x) that is to be added to 
                            the RHS of the heat equation                            

    Outputs
    ----------

    u_j                 :   numpy.array 
                            The range of solutions for the parabolic PDE over the given 
                            domain

    """

    # initialise array for u at time step j+1
    u_jp1 = np.zeros(x.size)    

    if boundary_conditions == "NH_Dirichlet":

        A_tridiag = diags([[lmbda], [1-2*lmbda], [lmbda]], [-1,0,1], shape=(mx-1,mx-1),format='csr')

        # Solve the PDE: loop over all time points
        for n in range(1, int(mt)+1):
            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[n],mx)            
            
            u_jp1[1:-1] = A_tridiag.dot(u_j[1:-1]+np.dot(lmbda,bound_conds_array))
                           
            # Apply Boundary conditions 
            u_jp1[0] = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[n],mx)[0]
            u_jp1[-1] = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[n],mx)[-1]

            # Update u_j
            u_j = u_jp1

    if boundary_conditions == "Neumann":       
        
        A_tridiag = diags([[lmbda], [1-2*lmbda], [lmbda]], [-1,0,1], shape=(mx+1,mx+1),format='csr')
        A_tridiag[0,1] = 2*A_tridiag[0,1]; A_tridiag[-1,-2] = 2*A_tridiag[-1,-2];

        # Solve the PDE: loop over all time points
        for n in range(1, int(mt)+1):
            # # Forward Euler timestep at inner mesh points
            # for i in range(1, mx):
            #     u_jp1[i] = u_j[i] + lmbda*(u_j[i-1] - 2*u_j[i] + u_j[i+1])  + (t[1]-t[0])*heat_source_generation(heat_source,x[i],t[n],mx)
             
            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[n],mx)            
            bound_conds_array[0] = -bound_conds_array[0]
            u_jp1 = A_tridiag.dot(u_j)+np.dot(2*lmbda*(L/mx),bound_conds_array)
                           
            # Apply Boundary conditions 
            # u_jp1[0] = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[n],mx)[0]
            # u_jp1[-1] = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[n],mx)[-1]

            # Update u_j
            u_j[:] = u_jp1[:]            

    return u_j

def backwardEuler(x, t, mx, mt, lmbda, L, T, u_j, initial_boundary, final_boundary, boundary_conditions, heat_source):

    """    
    A function that applies the forward Euler numerical scheme.

    Inputs
    ----------
    x                   :   list
                            The array of x values that are used to solve the PDE, coming 
                            from the domain.

    t                   :   list
                            The array of time values that are used to solve the PDE.

    mx                  :   int
                            The number of discretisation points/grid spacings in space.

    mt                  :   int
                            The number of discretisation points/grid spacings in time.

    lmbda               :   float
                            The mesh fourier number.

    L                   :   float
                            The size of the domain.

    T                   :   float
                            The desired computational time.

    u_j                 :   numpy.array 
                            The initial range of values for the parabolic PDE.

    initial_boundary    :   int/float/Function Handle
                            The boundary condition for the initial value of the system (p).

    final_boundary      :   int/float/Function Handle
                            The boundary condition for the final value of the system (q).

    boundary_conditions :   string
                            Chooes the type of boundary conditions used in the scheme.
                            "NH_Dirichlet": Applies the non-homogenous Dirichlet boundary
                                            conditions
                            "Neumann":      Applies the Neumann boundary conditions  

    heat_source         :   Function Handle
                            Function containing the function F(x) that is to be added to 
                            the RHS of the heat equation                            

    Outputs
    ----------

    u_j                 :   numpy.array 
                            The range of solutions for the parabolic PDE over the given 
                            domain

    """

    # initialise array for u at time step j+1
    u_jp1 = np.zeros(x.size)        
    
    if boundary_conditions == "NH_Dirichlet":
        # set up the sparse matrix for A
        A_tridiag = diags([[-lmbda], [1+2*lmbda], [-lmbda]], [-1,0,1], shape=(mx-1,mx-1),format='csr')

        # backward Euler scheme
        for i in range(1, mt+1):

            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[i],mx)

            u_jp1[1:-1] = spsolve(A_tridiag,u_j[1:-1]+np.dot(lmbda,bound_conds_array))
            # u_jp1[1:-1] = thomasSolver([-lmbda], [1+2*lmbda], [-lmbda],u_j[1:-1])      

            # Apply Boundary conditions
            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[i],mx)
            u_jp1[0] = bound_conds_array[0]; u_jp1[-1] = bound_conds_array[-1];
            
            # Update u_j
            u_j = u_jp1

    if boundary_conditions == "Neumann":
        # set up the sparse matrix for A
        A_tridiag = diags([[-lmbda], [1+2*lmbda], [-lmbda]], [-1,0,1], shape=(mx+1,mx+1),format='csr')
        A_tridiag[0,1] = 2*A_tridiag[0,1]; A_tridiag[-1,-2] = 2*A_tridiag[-1,-2];
        # backward Euler scheme
        for i in range(1, mt+1):

            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[i],mx)
            bound_conds_array[0] = -bound_conds_array[0]

            u_jp1 = spsolve(A_tridiag,u_j+np.dot(2*lmbda*(L/mx),bound_conds_array))
            
            # Update u_j
            u_j = u_jp1

    return u_jp1

def crankNicholson(x, t, mx, mt, lmbda, L, T, u_j, initial_boundary, final_boundary, boundary_conditions, heat_source):

    """    
    A function that applies the forward Euler numerical scheme.

    Inputs
    ----------
    x                   :   list
                            The array of x values that are used to solve the PDE, coming 
                            from the domain.

    t                   :   list
                            The array of time values that are used to solve the PDE.

    mx                  :   int
                            The number of discretisation points/grid spacings in space.

    mt                  :   int
                            The number of discretisation points/grid spacings in time.

    lmbda               :   float
                            The mesh fourier number.

    L                   :   float
                            The size of the domain.

    T                   :   float
                            The desired computational time.

    u_j                 :   numpy.array 
                            The initial range of values for the parabolic PDE.

    initial_boundary    :   int/float/Function Handle
                            The boundary condition for the initial value of the system (p).

    final_boundary      :   int/float/Function Handle
                            The boundary condition for the final value of the system (q).

    boundary_conditions :   string
                            Chooes the type of boundary conditions used in the scheme.
                            "NH_Dirichlet": Applies the non-homogenous Dirichlet boundary
                                            conditions
                            "Neumann":      Applies the Neumann boundary conditions  

    heat_source         :   Function Handle
                            Function containing the function F(x) that is to be added to 
                            the RHS of the heat equation                            

    Outputs
    ----------

    u_j                 :   numpy.array 
                            The range of solutions for the parabolic PDE over the given 
                            domain

    """

    # initialise array for u at time step j+1
    u_jp1 = np.zeros(x.size) 

    if boundary_conditions == "NH_Dirichlet":

        # set up the sparse matrices for A and B
        A_tridiag = diags([[-lmbda/2], [1+lmbda], [-lmbda/2]], [-1,0,1], shape=(mx-1,mx-1),format='csr')
        B_tridiag = diags([[lmbda/2], [1-lmbda], [lmbda/2]], [-1,0,1], shape=(mx-1,mx-1),format='csr')

        for i in range(1, mt+1):

            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,t[i],mx)
            
            u_jp1[1:-1] = spsolve(A_tridiag,(B_tridiag.dot(u_j[1:-1])+np.dot(lmbda,bound_conds_array)))
            # u_jp1[1:-1] = thomasSolver([-lmbda/2], [1+lmbda], [-lmbda/2],B_tridiag.dot(u_j[1:-1]) + lmbda * bound_conds_array) 
            
            u_j = u_jp1; u_j[0] = bound_conds_array[0]; u_j[-1] = bound_conds_array[-1];

    elif boundary_conditions == "Neumann":

        # set up the sparse matrices for A and B
        A_tridiag = diags([[-lmbda/2], [1+lmbda], [-lmbda/2]], [-1,0,1], shape=(mx+1,mx+1),format='csr')
        B_tridiag = diags([[lmbda/2], [1-lmbda], [lmbda/2]], [-1,0,1], shape=(mx+1,mx+1),format='csr')
        # Setting up the values of????
        A_tridiag[0,1] = 2*A_tridiag[0,1]; A_tridiag[-1,-2] = 2*A_tridiag[-1,-2];
        B_tridiag[0,1] = 2*B_tridiag[0,1]; B_tridiag[-1,-2] = 2*B_tridiag[-1,-2];

        for i in range(1, mt+1):

            T_j = i * T/mt
            T_jp1 = (i+1) * T/mt
            # setting up the array containing the chosen boundary conditions
                
            bound_conds_array = boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,T_jp1,mx)#+\
                                #boundaryCondsGenerator(boundary_conditions,initial_boundary,final_boundary,x,T_j,mx)
            bound_conds_array[0] = -bound_conds_array[0]
            # evaluating u at the next time step
            u_jp1 = spsolve(A_tridiag,(B_tridiag.dot(u_j)+np.dot(2*lmbda*(L/mx),bound_conds_array)))

            #u_jp1 = thomasSolver(-lmbda/2, 1+lmbda, -lmbda/2,(B_tridiag.dot(u_j)+np.dot(2*lmbda*(L/mx),bound_conds_array))) 
            u_j = u_jp1; #u_j[0] = bound_conds_array[0]; u_j[-1] = bound_conds_array[-1];



    return u_j

##################################################
############## BOUNDARY CONDITIONS ###############
##################################################

def boundaryCondsGenerator(boundary_conditions, initial_boundary, final_boundary, x, t, mx):

    # choosing the length of the boundary condition vector
    if boundary_conditions == "NH_Dirichlet":
        length = mx-1
    elif boundary_conditions == "Neumann":
        length = mx+1
    else:
        print("Error: Input boundary_conditions is not accepted.")
        print("Accepted formats: \"NH_Dirichlet\" , \"Neumann\"")
        sys.exit(1)

    Bounds = np.zeros(length); Bounds[0] = boundaryCondsType(initial_boundary,x,t,mx,boundary_conditions)[0]; Bounds[-1] = boundaryCondsType(final_boundary,x,t,mx,boundary_conditions)[-1];

    return Bounds

def boundaryCondsType(boundary,x,t,mx,boundary_conditions):

    """    
    A function that produces the heat source value to be applied to the
    RHS of the heat equation for each iteration of the numerical scheme.

    Inputs
    ----------
    
    heat_source         :   Function Handle
                            Function containing the function F(x) that is to be added to 
                            the RHS of the heat equation   

    x                   :   list
                            The array of x values that are used to solve the PDE, coming 
                            from the domain.

    t                   :   list
                            The array of time values that are used to solve the PDE.

    mx                  :   int
                            The number of discretisation points/grid spacings in space.

    Outputs
    ----------

    heat_source_val     :   float 
                            The range of solutions for the parabolic PDE over the given 
                            domain

    """

    # if boundaries are functions
    if callable(boundary) == True:
        p = boundary(x,t,mx)     # sets value for initial boundary
    elif type(boundary) == list or type(boundary) == np.ndarray:
        p = boundary # sets value for initial boundary
    elif type(boundary) == float or type(boundary) == int:
        p = [boundary]                # sets value for initial boundary
    else:
        print("Error: boundary input of an incorrect datatype.")
        sys.exit(1)

    return p

##################################################
################## HEAT SOURCE ###################
##################################################

def u_exact(x, t, L, kappa):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y

def heat_source_generation(heat_source, x, t, mx):

    """    
    A function that produces the heat source value to be applied to the
    RHS of the heat equation for each iteration of the numerical scheme.

    Inputs
    ----------
    
    heat_source         :   Function Handle
                            Function containing the function F(x) that is to be added to 
                            the RHS of the heat equation   

    x                   :   list
                            The array of x values that are used to solve the PDE, coming 
                            from the domain.

    t                   :   list
                            The array of time values that are used to solve the PDE.

    mx                  :   int
                            The number of discretisation points/grid spacings in space.

    Outputs
    ----------

    heat_source_val     :   float 
                            The range of solutions for the parabolic PDE over the given 
                            domain

    """

    if heat_source == None:
        # If no heat source function is provided
        heat_source_val = 0
    elif callable(heat_source) == True:
        heat_source_val = heat_source(x,t,mx)
    else:
        print("Error: Input heat_source is of an invalid format. Should be a function handle.")
        sys.exit(1)

    return heat_source_val
 
##################################################
################# MAIN FUNCTION ##################
##################################################

def PDEsolver(  diff_coefficient, 
                L,
                T, 
                init_temp_dist, 
                mx,
                mt,
                initial_boundary,
                final_boundary,
                boundary_conditions,
                scheme=crankNicholson,
                heat_source=None,
                plot = False
                ):

    """    
    A function that solves a parabolic partial differential equation. EXPAND

    Inputs
    ----------
    diff_coefficient    :   float
                            The value of the diffusion constant (kappa) used in the heat 
                            equation.

    L                   :   float
                            The size of the domain.

    T                   :   float
                            The desired computational time.

    init_temp_dist      :   Function Handle
                            The handle of the function containing the initial temperature
                            distribution.

    mx                  :   int
                            The number of discretisation points/grid spacings in space.

    mt                  :   int
                            The number of discretisation points/grid spacings in time.

    initial_boundary    :   int/float/Function Handle
                            The boundary condition for the initial value of the system (p)

    final_boundary      :   int/float/Function Handle
                            The boundary condition for the final value of the system (q)

    boundary_conditions :   string
                            Chooes the type of boundary conditions used in the scheme.
                            "NH_Dirichlet": Applies the non-homogenous Dirichlet boundary
                                            conditions
                            "Neumann":      Applies the Neumann boundary conditions  

    scheme              :   Function Handle 
                            forwardEuler: Forwards Euler numerical scheme
                            backwardEuler: Backwards Euler numerical scheme
                            crankNicholson: Crank Nicholson numerical scheme

    heat_source         :   Function Handle
                            Function containing the function F(x) that is to be added to 
                            the RHS of the heat equation

    plot                :   True/False
                            Choice whether to plot a graph showing the calculated solution
                            to the PDE (u_j) against x
                            

    Outputs
    ----------
    x                   :   numpy.array 
                            The domain that the PDE is solved over

    u_j                 :   numpy.array 
                            The range of solutions for the parabolic PDE over the given 
                            domain

    """

    # Set up the numerical environment variables
    x = np.linspace(0, L, mx+1)                     # mesh points in space
    t = np.linspace(0, T, mt+1)                     # mesh points in time
    deltax = x[1] - x[0]                            # gridspacing in x
    deltat = t[1] - t[0]                            # gridspacing in t
    lmbda = diff_coefficient*deltat/(deltax**2)     # mesh fourier number

    u_initial = np.zeros(x.size)                    # set initial condition
    for i in range(0, mx+1):
        u_initial[i] = init_temp_dist(x[i], L)

    # Numerical scheme Selection
    if scheme == forwardEuler:
        # Forwards Euler
        u_j = forwardEuler(x, t, mx, mt, lmbda, L, T, u_initial, initial_boundary, final_boundary, boundary_conditions, heat_source)

    elif scheme == backwardEuler:
        # Backwards Euler
        u_j = backwardEuler(x, t, mx, mt, lmbda, L, T, u_initial, initial_boundary, final_boundary, boundary_conditions, heat_source)

    elif scheme == crankNicholson:
        # Crank Nicholson
        u_j = crankNicholson(x, t, mx, mt, lmbda, L, T, u_initial, initial_boundary, final_boundary, boundary_conditions, heat_source)   

    else:
        # Error message given when an unrecognised scheme is inputted
        print("Error: Incorect Numerical Scheme Input") 
        return("Scheme Error")
        sys.exit(1)  

    # Plotting capability
    if plot == True:    
        fig1 = plt.figure(figsize = [6,5])
        fig1.tight_layout()
        ax1 = fig1.add_subplot(1,1,1)
        ax1.plot(x,u_j,'ro',label='num')
        # Option to plot an exact solution to the PDE for comparison
        ax1.plot(np.linspace(0,L,250),u_exact(np.linspace(0,L,250), T, L, diff_coefficient),'b-',label='exact')
        ax1.set_title("Wave Equation Solution")
        ax1.set_xlabel('x')
        ax1.set_ylabel('u(x,0.5)')
        # ax1.legend(loc='upper right')
        plt.show()

    return x, u_j


