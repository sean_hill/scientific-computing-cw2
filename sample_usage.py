# Written by Sean P. Hill (sh17512@bristol.ac.uk) 2019

# This is a function that provides a working example of how to use
# pdeSolver.py.

import numpy as np
import pylab as plt
from math import pi
from scipy.sparse import diags
from numpy.linalg import inv
from numpy.linalg import solve as linalgSolver
import sys
from pdeSolver import * # This is how the function can be imported

###################################################################
# Examples of functions that can be used to evaluate the solution 
# of the PDE, as well as work out the errors from the exact 
# solution.

def u_I(x, L):
    # initial temperature distribution
    y = np.sin(pi*x/L) 
    return y

def u_exact(x, t, L, kappa):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y

def heat_source_test(x,t,mx):
	# 
    return 2*x*t

def test_intial_bound_conds(x,t,mxs):
    
    return [np.sin(pi*-2*t)]

def test_final_bound_conds(x,t,mxs):
    
    return [np.sin(pi*2*t)]

###################################################################
# Example of how the function can be ran, wwith referencing the 
# functions relevant to the scheme and boundary conditions that
# the user wants to input.

x,u = PDEsolver(diff_coefficient=1.0,
				L=1.0,
				T=0.5, 
				init_temp_dist=u_I,
				mx=20,
				mt=1000,
				initial_boundary = 0,
				final_boundary = 0,
            	boundary_conditions = "NH_Dirichlet",
            	scheme = crankNicholson,
            	heat_source = None,
            	plot = False
		        )

###################################################################

fig1 = plt.figure(figsize = [6,5])
fig1.tight_layout()
ax1 = fig1.add_subplot(1,1,1)
ax1.plot(x,u,'yo-',label='Crank Nicholson')
# Option to plot an exact solution to the PDE for comparison
ax1.plot(np.linspace(0,1,250),u_exact(np.linspace(0,1,250), 0.5, 1, 1),'b-',label='exact')
fig1.suptitle("Wave Equation Solution For Dirichlet Boundary Conditions = 0")
ax1.set_xlabel('x')
ax1.set_ylabel('u(x,0.5)')
# ax1.set_yticklabels(labels = [0.48,0.49,0.5,0.51,0.52,0.53,0.54])
ax1.legend(loc='upper left')
plt.show()









