#################################################
##################### USAGE #####################
#################################################
"""
This is a test file that tests the pdeSolver.py
library.
"""

import numpy as np
import pylab as plt
from math import pi
from scipy.sparse import diags
from numpy.linalg import inv
from numpy.linalg import solve as linalgSolver
import sys
from pdeSolver import *

##################################################
################# TEST FUNCTIONS #################
##################################################

def u_I(x, L):
    # initial temperature distribution
    y = np.sin(pi*x/L) 
    return y

def u_exact(x, t, L, kappa):
    # the exact solution
    y = np.exp(-kappa*(pi**2/L**2)*t)*np.sin(pi*x/L)
    return y

def heat_source_test(x,t,mx):

    return 2*x*t

def test_intial_bound_conds(x,t,mxs):
    
    return [np.sin(pi*2*t)]

def test_final_bound_conds(x,t,mxs):
    
    return [np.sin(pi*2*t)]

##################################################
##################### TESTS ######################
##################################################

No_Tests = 0
Tests_Passed = 0
Tests_Failed = 0
Tolerance = 0.05

diff_coefficient=1.0
L=1.
T=0.5 
init_temp_dist=u_I
mx=30
mt=1000
initial_boundary = 0
final_boundary = 0

print("\n===========================================================")
print('TEST 1: Testing running with incorrect numerical scheme input\n')


test_1 = PDEsolver(	diff_coefficient, 
            L,
            T, 
            init_temp_dist,
            mx,
            mt,
            initial_boundary,
            final_boundary,
            boundary_conditions = "Neumann",
            scheme = "crankNicholson1",
            heat_source = None,
            plot = False
		        )

if test_1 == "Scheme Error":
	print('RESULT:   Test passed')
	Tests_Passed += 1
	No_Tests+=1
else:
	print('RESULT:   Test failed')
	Tests_Failed += 1
	No_Tests+=1

print("===========================================================")

print('TEST 2: Testing Error for Forwards Euler Against Exact Solution\n')


x_test2, test_2 = PDEsolver(	diff_coefficient, 
            L,
            T, 
            init_temp_dist,
            mx,
            mt,
            initial_boundary=0,
            final_boundary=0,
            boundary_conditions = "NH_Dirichlet",
            scheme = forwardEuler,
            heat_source = None,
            plot = False
		        )

exact_sol2 = u_exact(np.linspace(0,L,len(test_2)), T, L, diff_coefficient)
test2_outcome = np.allclose(test_2,exact_sol2,rtol=Tolerance)


if test2_outcome == True:
	print('RESULT:   Test passed')
	Tests_Passed += 1
	No_Tests+=1
else:
	print('RESULT:   Test failed')
	Tests_Failed += 1
	No_Tests+=1

print("===========================================================")

print('TEST 3: Testing Error for Backwards Euler Against Exact Solution\n')


x_test3, test_3 = PDEsolver(	diff_coefficient, 
            L,
            T, 
            init_temp_dist,
            mx,
            mt,
            initial_boundary=0,
            final_boundary=0,
            boundary_conditions = "NH_Dirichlet",
            scheme = backwardEuler,
            heat_source = None,
            plot = False
		        )

exact_sol3 = u_exact(np.linspace(0,L,len(test_3)), T, L, diff_coefficient)
test3_outcome = np.allclose(test_3,exact_sol3,rtol=Tolerance)


if test3_outcome == True:
	print('RESULT:   Test passed')
	Tests_Passed += 1
	No_Tests+=1
else:
	print('RESULT:   Test failed')
	Tests_Failed += 1
	No_Tests+=1

print("===========================================================")

print('TEST 4: Testing Error for Crank Nicholson Against Exact Solution\n')


x_test4, test_4 = PDEsolver(	diff_coefficient, 
            L,
            T, 
            init_temp_dist,
            mx,
            mt,
            initial_boundary=0,
            final_boundary=0,
            boundary_conditions = "NH_Dirichlet",
            scheme = crankNicholson,
            heat_source = None,
            plot = False
		        )

exact_sol4 = u_exact(np.linspace(0,L,len(test_4)), T, L, diff_coefficient)
test4_outcome = np.allclose(test_4,exact_sol4,rtol=Tolerance)


if test4_outcome == True:
	print('RESULT:   Test passed')
	Tests_Passed += 1
	No_Tests+=1
else:
	print('RESULT:   Test failed')
	Tests_Failed += 1
	No_Tests+=1

print("===========================================================")



print("Tests Completed:",No_Tests)
print("%i/%i Tests Passed\n"%(Tests_Passed,No_Tests))
